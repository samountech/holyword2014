//
//  CReaderObjc.h
//  VoiceGo3
//
//  Created by tai on 2012/7/25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

//=====================================================================================================================
// Import
//=====================================================================================================================
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

//=====================================================================================================================
// Macro
//=====================================================================================================================
// Error Code
#define CREADER_SUCCESS                         ( 0 )

#define CREADER_ERROR_EXPIRED					( -100 )
#define CREADER_ERROR_INVALID_LANG				( -101 )
#define CREADER_ERROR_BINS_NOT_FOUND			( -102 )
#define CREADER_ERROR_INIT_ENGINE_FAIL			( -103 )
#define CREADER_ERROR_PARAMETER_FAIL			( -104 )
#define CREADER_ERROR_SAMPLERATE_FAIL			( -105 )
#define CREADER_ERROR_DATASIZE_FAIL             ( -106 )
#define CREADER_ERROR_MALLOC_FAIL               ( -107 )
#define CREADER_ERROR_LICENSE_FAIL              ( -108 )

// CReader Status
#define CREADER_STATE_STOP                      ( -201 )
#define CREADER_STATE_PLAY                      ( -202 )
#define CREADER_STATE_PAUSE                     ( -203 )

// Audio Item types
#define CREADER_AUDIO_TYPE_TEXT                 ( -301 )
#define CREADER_AUDIO_TYPE_FILE                 ( -302 )
#define CREADER_AUDIO_TYPE_BUFFER               ( -303 )
#define CREADER_AUDIO_TYPE_SILENCE              ( -304 )


@interface CReaderObjc : NSObject

//=====================================================================================================================
// Property
//=====================================================================================================================
@property (nonatomic, weak)	id delegate;
@property (nonatomic, assign)	int pitch;
@property (nonatomic, assign)	int speed;
@property (nonatomic, assign)	int volume;
@property (nonatomic, readonly) int ttsStatus;
@property (weak, nonatomic, readonly) NSString *speaker;
@property (weak, nonatomic, readonly) NSString *language;

//=====================================================================================================================
// CReader Public API
//=====================================================================================================================
/**
 * @brief Initialize the CReader object.
 * @param ttsDir The full path of language data folder.
 * @param nlzName The file name of nlz data, you can define a customize rule on TTS, it can be nil
 * @param lang The language which is used to create corresponding TTS. (for ex: "zh-TW", "en-US")
 * @param speakerName The TTS speaker name. (for ex: "Ally", "Anita")
 * @param err If initialization succeeds, return NULL, else return error information. More information please refer to "CReader Error Code"
 * @return If initialization CReader succeeds, return CReader Object, else return nil.
 */
- (id)initWithBinPath:(NSString *)ttsDir
                  nlz:(NSString *)nlzName
             language:(NSString *)lang
              speaker:(NSString *)speakerName
                error:(NSError **)err;

/**
 * @brief Add an audio item to the CReader. It can be a text string to be synthesized, a file or a memory buffer of audio data, and a silence of specified duration.
 * @param item If audio item is text, it has to be NSString object.If audio item is file, it has to be NSString object.If audio item is buffer, it has to be NSData object.If audio item is silence, it has to be NSNumber object.
 * @param type Audio item type, it is only set to the following constants: CREADER_ AUDIO_TYPE_TEXT,CREADER_ AUDIO_TYPE_FILE,CREADER_ AUDIO_TYPE_BUFFER andCREADER_ AUDIO_TYPE_SILENCE.
 * @param isFadeout Set it YES to gradually decrease the volume while playing at the end of the audio item, otherwise set it NO.
 * @return If the audio item is successfully added, return CREADER_SUCCESS, else return error code.
 * @warning If item is a wave file path or a memory buffer, the audio data has to be in 8kHz or 16kHz sampling rate, 16-bit, single-channel, uncompressed PCM samples.
 */
- (int)addAudioItem:(id)item
      withAudioType:(int)type
          isFadeout:(BOOL)isFadeout;

/**
 * @brief Clear all the audio items added previously.
 * @return If audio items are successfully cleared, return CREADER_SUCCESS, else return error code.
 */
- (int)clearAudioItems;

/**
 * @brief Start playing the added audio items in the background, or to resume the playback. If you have set delegate in CReader, it will to call delegate functions with main thread.
 */
- (void)play;

/**
 * @brief Pause playback.
 */
- (void)pause;

/**
 * @brief Manually stop the playback.
 */
- (void)abort;

/**
 * @brief Manually stop the playback with gradually decreasing volume.
 */
- (void)abortFadeOut;

/**
 * @brief Set the talking speed of TTS, ranging from 50(slowest) to 200(fastest), default value is 100.
 * @param speed The TTS speed.
 */
- (void)setSpeed:(int)speed;

/**
 * @brief Set the volume of TTS, ranging from 0(silent) to 500(loud), default value is 100.
 * @param volume The TTS volume.
 */
- (void)setVolume:(int)volume;

/**
 * @brief Set the frequency of the TTS speaker, ranging from 50(low) to 200(high), default value is 100.
 * @param pitch The frequency of the TTS speaker.
 */
- (void)setPitch:(int)pitch;

/**
 * @brief Set pause time delay for punctuation marks (in milliseconds).
 * @param punctMarks The string of punctuation marks, for example, @î,??!:;î..
 * @param nDelay The time delay in millisecond.
 */
- (void)setPuncMark:(NSString*)punctMarks
              delay:(int)nDelay;

/**
 * @brief Reset the pause time delay for punctuation marks to default.
 */
- (void)resetPuncMarkDelay;

/**
 * @brief Depending on different languages, CReader may take considerable amount of memory. However, the memory may not always be in use, and sometimes it can be released temporarily. When the process runs out of memory, this function can be called to release the memory taken by CReader temporarily. The memory will be automatically allocated again next time when CReader functions are called. 
 */
- (void)processLowMemoryWarning;

//=====================================================================================================================
// CReader Unpublic API
//=====================================================================================================================
- (void)initPlayText:(NSString*)inText isFadeoutAtEnd:(BOOL)flag;
- (void)backward:(int)minLength;
- (void)backward;

@end
//=====================================================================================================================
// CReader Delegate
//=====================================================================================================================
@protocol CReaderDelegate
@optional
/**
 * @brief This function gets called when CReader is about to begin playback.
 * @param The CReader instance.
 */
- (void)onCReaderTtsBegin:(CReaderObjc*)creader;

/**
 * @brief This function gets called when CReader finishes playback.
 * @param The CReader instance.
 */
- (void)onCReaderTtsFinish:(CReaderObjc*)creader;

/**
 * @brief For evaluation version, at the initialization stage, if the evaluation date is expire, this function will be called with the expiration date.
 * @param date The date of expired time.
 */
- (void)onCReaderExpire:(NSDate*)date;

@end
