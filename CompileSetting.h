//
//  CompileSetting.h
//  CReaderDemo
//
//  Created by CGHao on 2014/9/17.
//
//

#ifndef CReaderDemo_CompileSetting_h
#define CReaderDemo_CompileSetting_h


//=========================================================
// Debug Mode for CReader Demo
//#define _Demo_Debug_
//=========================================================


#ifdef _Demo_Debug_
#define MyNSLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define MyNSLog(format, ...)
#endif


#endif
